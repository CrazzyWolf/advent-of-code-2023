﻿namespace Advent_of_Code_2023
{
    internal class Day07Part1 : IDayPart
    {
        public enum HandType
        {
            HighCard,
            OnePair,
            TwoPair,
            ThreeOfAKind,
            FullHouse,
            FourOfAKind,
            FiveOfAKind,
        }

        class HandComparer : IComparer<(string hand, HandType type)>
        {
            static readonly Dictionary<char, int> cardTypes = new()
            {
                { '2', 0 },
                { '3', 1 },
                { '4', 2 },
                { '5', 3 },
                { '6', 4 },
                { '7', 5 },
                { '8', 6 },
                { '9', 7 },
                { 'T', 8 },
                { 'J', 9 },
                { 'Q', 10 },
                { 'K', 11 },
                { 'A', 12 },
            };

            public int Compare((string hand, HandType type) a, (string hand, HandType type) b)
            {
                int c1 = a.type.CompareTo(b.type);
                if(c1 != 0)
                {
                    return c1;
                }

                string aHand = a.hand, bHand = b.hand;
                for(int i = 0; i < aHand.Length; i++)
                {
                    int c2 = cardTypes[aHand[i]] - cardTypes[bHand[i]];
                    if(c2 != 0)
                    {
                        return c2;
                    }
                }

                return 0;
            }
        }

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day07/input.tar"); //example.txt

            List<(string hand, int bid, HandType type)> hands = [];

            foreach(string line in input)
            {
                string[] split = line.Split(" ");
                string hand = split[0];
                hands.Add((hand, int.Parse(split[1]), GetHandType(hand)));
            }

            int[] orderedBids = hands.OrderBy(x => (x.hand, x.type), new HandComparer()).Select(x => x.bid).ToArray();

            int result = 0;
            for(int rank = 1; rank <= orderedBids.Length; rank++)
            {
                result += rank * orderedBids[rank - 1];
            }

            Console.WriteLine(result);
        }

        private static HandType GetHandType(string hand)
        {
            Dictionary<char, int> dict = [];
            foreach(char card in hand)
            {
                dict.TryGetValue(card, out int value);
                dict[card] = value + 1;
            }

            return dict.Max(x => x.Value) switch
            {
                5 => HandType.FiveOfAKind,
                4 => HandType.FourOfAKind,
                3 => dict.Count == 2 ? HandType.FullHouse : HandType.ThreeOfAKind,
                2 => dict.Count == 3 ? HandType.TwoPair : HandType.OnePair,
                _ => HandType.HighCard
            };
        }
    }
}

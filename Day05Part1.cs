﻿namespace Advent_of_Code_2023
{
    internal class Day05Part1 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day05/input.tar");

            long[] seeds = input[0].Remove(0, 7).Split(" ").Select(long.Parse).ToArray();
            long[] resultList = seeds;
            List<List<(long, long, long)>> lists = [];
            List<(long, long, long)> tempList = [];

            for(int i = 3; i <= input.Length; i++)
            {
                if(i == input.Length || input[i].Length == 0)
                {
                    lists.Add(tempList);
                    tempList = [];
                    i++;
                }
                else
                {
                    long[] split = input[i].Split(" ").Select(long.Parse).ToArray();
                    tempList.Add((split[0], split[1], split[2]));
                }
            }

            for(int i = 0; i < seeds.Length; i++)
            {
                foreach(var map in lists)
                {
                    foreach((long dStart, long sStart, long range) in map)
                    {
                        long sEnd = sStart + range;
                        long r = resultList[i];

                        if(r >= sStart && r <= sEnd)
                        {
                            resultList[i] = r - sStart + dStart;
                            break;
                        }
                    }
                }
            }

            Console.WriteLine(resultList.Min());
        }
    }
}
﻿namespace Advent_of_Code_2023
{
    internal class Day08Part2 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day08/input.txt");
            Dictionary<string, (string, string)> nodes = [];
            char[] sequence = input[0].ToCharArray();

            for(int i = 2; i < input.Length; i++)
            {
                string line = input[i];
                nodes.Add(line[..3], (line.Substring(7, 3), line.Substring(12, 3)));
            }

            string[] keys = nodes.Where(x => x.Key[2] == 'A').Select(x => x.Key).ToArray();

            List<long> results = [];

            foreach(string k in keys)
            {
                int t = 0;
                string key = k;
                (string l, string r) = nodes[key];
                for(int i = 0; key[2] != 'Z'; i++)
                {
                    key = sequence[i % sequence.Length] == 'L' ? l : r;
                    (l, r) = nodes[key];
                    t++;
                }

                results.Add(t);
            }

            Console.WriteLine(results.Aggregate((a, b) => a * b / LeastCommonMultiple(a, b)));
        }

        private static long LeastCommonMultiple(long a, long b)
        {
            return b == 0 ? a : LeastCommonMultiple(b, a % b);
        }
    }
}

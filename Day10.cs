﻿namespace Advent_of_Code_2023
{
    internal class Day10 : IDayPart
    {
        enum Way
        {
            Up, Down, Left, Right,
        }

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day10/input.txt");

            Dictionary<(int x, int y), (Way w1, Way w2)> pipes = [];
            Dictionary<(int x, int y), int> loop = [];

            int maxY = input.Length - 1;
            int maxX = input[0].Length - 1;

            for(int y = 0; y <= maxY; y++)
            {
                string line = input[y];
                //Console.WriteLine(line);
                for(int x = 0; x <= maxX; x++)
                {
                    char c = line[x];
                    var key = (x, y);
                    if(c == 'S')
                    {
                        loop[key] = 0;
                    }
                    else if(c != '.')
                    {
                        pipes[key] = c switch
                        {
                            '|' => (Way.Up, Way.Down),
                            '-' => (Way.Left, Way.Right),
                            'L' => (Way.Up, Way.Right),
                            'J' => (Way.Left, Way.Up),
                            '7' => (Way.Left, Way.Down),
                            _ => (Way.Right, Way.Down),
                        };
                    }
                }
            }
            //Console.WriteLine();

            Queue<(int x, int y, int distance)> distanceQueue = [];
            (int startX, int startY) = loop.First().Key;

            if(pipes.TryGetValue((startX + 1, startY), out (Way w1, Way w2) pipeRight)
                && pipeRight.w1 == Way.Left)
            {
                distanceQueue.Enqueue((startX + 1, startY, 1));
            }
            if(pipes.TryGetValue((startX - 1, startY), out (Way w1, Way w2) pipeLeft) 
                && (pipeLeft.w1 == Way.Right || pipeLeft.w2 == Way.Right))
            {
                distanceQueue.Enqueue((startX - 1, startY, 1));
            }
            if(pipes.TryGetValue((startX, startY + 1), out (Way w1, Way w2) pipeDown)
                && (pipeDown.w1 == Way.Up || pipeDown.w2 == Way.Up))
            {
                distanceQueue.Enqueue((startX, startY + 1, 1));
            }
            if(pipes.TryGetValue((startX, startY - 1), out (Way w1, Way w2) pipeUp) 
                && pipeUp.w2 == Way.Down)
            {
                distanceQueue.Enqueue((startX, startY - 1, 1));
            }

            int part1 = 0;

            while(distanceQueue.Count > 0)
            {
                (int x, int y, int distance) = distanceQueue.Dequeue();
                var key = (x, y);

                var (w1, w2) = pipes[key];

                (int x1, int y1) = GetCoordinates(x, y, w1);
                (int x2, int y2) = GetCoordinates(x, y, w2);

                part1 = Math.Max(part1, distance);
                
                loop.TryAdd(key, distance++);

                if(!loop.ContainsKey((x1, y1)))
                {
                    distanceQueue.Enqueue((x1, y1, distance));
                }
                else if(!loop.ContainsKey((x2, y2)))
                {
                    distanceQueue.Enqueue((x2, y2, distance));
                }
            }

            Console.WriteLine(part1);

            int biggerMaxX = input[0].Length * 2;
            int biggerMaxY = input.Length * 2;
            char[,] biggerGrid = new char[biggerMaxX + 1, biggerMaxY + 1];

            foreach(var i in loop)
            {
                var (x, y) = i.Key;
                int biggerX = x * 2 + 1;
                int biggerY = y * 2 + 1;
                biggerGrid[biggerX, biggerY] = input[y][x];

                if(!pipes.TryGetValue((x, y), out var ways))
                {
                    continue; //skip S
                }

                (Way w1, Way w2) = ways;

                (int x1, int y1) = GetCoordinates(biggerX, biggerY, w1);
                (int x2, int y2) = GetCoordinates(biggerX, biggerY, w2);

                biggerGrid[x1, y1] = w1 == Way.Right || w1 == Way.Left ? '-' : '|';
                biggerGrid[x2, y2] = w2 == Way.Right || w2 == Way.Left ? '-' : '|';
            }

            HashSet<(int, int)> removed = [];
            Queue<(int, int)> removeQueue = [];
            removeQueue.Enqueue((0, 0));

            while(removeQueue.Count > 0)
            {
                (int x, int y) = removeQueue.Dequeue();

                var key = (x, y);
                if(!removed.Add(key))
                {
                    continue;
                }

                if(x > 0 && biggerGrid[x - 1, y] == '\0')
                {
                    removeQueue.Enqueue((x - 1, y));
                }
                if(x < biggerMaxX && biggerGrid[x + 1, y] == '\0')
                {
                    removeQueue.Enqueue((x + 1, y));
                }
                if(y > 0 && biggerGrid[x, y - 1] == '\0')
                {
                    removeQueue.Enqueue((x, y - 1));
                }
                if(y < biggerMaxY && biggerGrid[x, y + 1] == '\0')
                {
                    removeQueue.Enqueue((x, y + 1));
                }
            }

            /*
            Console.WriteLine();
            for(int y = 0; y <= biggerMaxY; y++)
            {
                for(int x = 0; x <= biggerMaxX; x++)
                {
                    Console.Write(removed.Contains((x, y)) ? "X" : biggerGrid[x, y] == '\0' ? ' ' : biggerGrid[x, y]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            */

            int part2 = 0;

            for(int y = 1; y <= biggerMaxY; y += 2)
            {
                for(int x = 1; x <= biggerMaxX; x += 2)
                {
                    if(!removed.Contains((x, y)))
                    {
                        part2++;
                    }
                }
            }

            Console.WriteLine(part2 - loop.Count);
        }

        private static (int x, int y) GetCoordinates(int x, int y, Way w)
        {
            return w switch
            {
                Way.Left => (x - 1, y),
                Way.Right => (x + 1, y),
                Way.Up => (x, y - 1),
                Way.Down => (x, y + 1),
                _ => throw new Exception(),
            };
        }
    }
}

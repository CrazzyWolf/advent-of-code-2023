﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advent_of_Code_2023
{
    internal class Day02Part2 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day02/input.txt");

            int result = 0;

            for(int gameIndex = 0; gameIndex < input.Length; gameIndex++)
            {
                string[] sets = input[gameIndex].Split(": ")[1].Split("; ");
                Dictionary<string, int> minColors = new()
                {
                    { "red", 0 },
                    { "green", 0 },
                    { "blue", 0 },
                };

                foreach(string set in sets)
                {
                    string[] colorAmount = set.Split(", ");
                    foreach (string color in colorAmount)
                    {
                        string[] colorSplit = color.Split(" ");
                        string key = colorSplit[1];
                        int amount = int.Parse(colorSplit[0]);
                        if(amount > minColors[key])
                        {
                            minColors[key] = amount;
                        }
                    }
                }

                int multiplier = 1;
                foreach(int color in minColors.Values)
                {
                    multiplier *= color;
                }
                result += multiplier;
            }

            Console.WriteLine(result);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;
using System;

namespace Advent_of_Code_2023
{
    internal class Day12 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day12/input.txt");
            long part1 = 0, part2 = 0;

            foreach(string line in input)
            {
                string[] split = line.Split(' ');
                string rowString = split[0];
                int[] groups = [.. split[1].Split(',').Select(int.Parse)];
                int groupSum = groups.Sum();

                part1 += DFS([], rowString, 0, groups, 0, 0, groupSum);
                part2 += DFS([], string.Join("?", rowString, rowString, rowString, rowString, rowString), 
                    0, [.. groups, .. groups, .. groups, .. groups, .. groups], 0, 0, groupSum * 5);
            }

            Console.WriteLine($"{part1}\n{part2}");
        }

        private static long DFS(Dictionary<(string row, int groupIndex, int count), long> cache, 
            string row, int start, int[] groups, int groupIndex, int broken, int brokenTotal)
        {
            var cacheKey = (row[start..], groupIndex, row.Count(a => a == '#'));

            if(cache.TryGetValue(cacheKey, out var v))
            {
                return v;
            }

            long result = 0;
            int group = groups[groupIndex];
            int nextGroupIndex = groupIndex + 1;

            if(groups.Length == nextGroupIndex)
            {
                result = Calc(row, start, group).Where(a => a.row.Count(c => c == '#') == brokenTotal).Count();
            }
            else
            {
                HashSet<(string row, int start)> visited = [];
                int length = row.Length - groups.Length + groupIndex - 1;

                for(int i = start; i < length; i++)
                {
                    if(row[..i].Count(a => a == '#') == broken)
                    {
                        foreach(var a in Calc(row, i, group))
                        {
                            if(visited.Add((a.row, a.start)))
                            {
                                result += DFS(cache, a.row, a.start, groups, 
                                    nextGroupIndex, broken + group, brokenTotal);
                            }
                        }
                    }
                }
            }

            cache.Add(cacheKey, result);
            return result;
        }

        private static List<(string row, int start)> Calc(string row, int start, int group)
        {
            string groupString = string.Concat(Enumerable.Repeat("#", group));
            List<(string row, int start)> arrangements = [];
            int max = row.Length - group;

            for(int startIndex = start; startIndex <= max; startIndex++)
            {
                int length = startIndex + group;

                if(startIndex > 0 && row[startIndex - 1] == '#'
                    || length <= max && row[length] == '#'
                    || row[startIndex..length].Contains('.'))
                {
                    continue;
                }

                arrangements.Add((row.Remove(startIndex, group).Insert(startIndex, groupString), length));
            }

            return arrangements;
        }
    }
}

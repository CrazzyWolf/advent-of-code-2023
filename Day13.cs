﻿namespace Advent_of_Code_2023
{
    internal class Day13 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day13/input.txt");
            List<string> map = [];
            int part1 = 0, part2 = 0, h, v, temp;

            foreach(string line in input)
            {
                if(line.Length == 0)
                {
                    v = 0;
                    part1 += (h = Find(map)) > 0 ? h * 100 : (v = Find(FlipMap(map)));

                    for(int i = 0; i < map.Count; i++)
                    {
                        for(int j = 0; j < map[0].Length; j++)
                        {
                            List<string> newMap = new(map);
                            string l = newMap[i];
                            newMap[i] = l.Remove(j, 1).Insert(j, l[j] == '.' ? "#" : ".");
                            if((temp = 100 * Find(newMap, h)) != 0 || (temp = Find(FlipMap(newMap), v)) != 0)
                            {
                                part2 += temp;
                                goto done;
                            }
                        }
                    }

                    done:
                    map = [];
                }
                else
                {
                    map.Add(line);
                }
            }

            Console.WriteLine(part1 + "\n" + part2);
        }

        private static List<string> FlipMap(List<string> map)
        {
            List<string> map2 = [];
            int length = map.Count;
            int length2 = map[0].Length;

            for(int i = 0; i < length2; i++)
            {
                string line = "";
                for(int j = 0; j < length; j++)
                {
                    line += map[j][i];
                }
                map2.Add(line);
            }

            return map2;
        }

        private static int Find(List<string> lines, int skip = 0)
        {
            List<int> options = [];

            for(int i = 1; i < lines.Count; i++)
            {
                if(lines[i - 1] == lines[i] && skip != i)
                {
                    options.Add(i);
                }
            }

            foreach(int index in options)
            {
                int i = index - 1, j = index, k = 1, max = lines.Count - 1;

                while(i > 0 && j < max)
                {
                    if(lines[--i] != lines[++j])
                    {
                        goto skip;
                    }
                    k++;
                }

                return index;
                skip:;
            }

            return 0;
        }
    }
}

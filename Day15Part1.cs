﻿namespace Advent_of_Code_2023
{
    internal class Day15Part1 : IDayPart
    {
        public static void Start()
        {
            Console.WriteLine(File.ReadAllLines("../../../Day15/input.txt")[0].Split(",").Sum(s => s.Aggregate(0, (a, b) => 17 * (a + b) % 256)));
        }
    }
}

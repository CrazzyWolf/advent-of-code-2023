﻿namespace Advent_of_Code_2023
{
    internal class Day14Part1 : IDayPart
    {
        public static void Start()
        {
            char[][] grid = File.ReadAllLines("../../../Day14/input.txt").Select(a => a.ToCharArray()).ToArray();
            int length = grid.Length;
            int length2 = grid[0].Length;

            for(int i = 0; i < length; i++)
            {
                for(int j = 0; j < length2; j++)
                {
                    if(grid[i][j] == 'O')
                    {
                        int k = i;
                        while(k > 0 && grid[k - 1][j] == '.')
                        {
                            k--;
                        }
                        grid[i][j] = '.';
                        grid[k][j] = 'O';
                    }
                }
            }

            int result = 0;
            for(int i = 0; i < length; i++)
            {
                for(int j = 0; j < length2; j++)
                {
                    if(grid[i][j] == 'O')
                    {
                        result += length - i;
                    }
                }
            }
            Console.WriteLine(result);
        }
    }
}

﻿namespace Advent_of_Code_2023
{
    internal class Day01Part1 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day01/input.txt");
            int result = 0;

            foreach (string line in input)
            {
                char start = '?', end = '?';
                bool startSet = false, endSet = false;
                int length = line.Length;
                int last = length - 1;
                for(int i = 0; i < length; i++)
                {
                    if(!startSet)
                    {
                        char temp = line[i];
                        if(Char.IsNumber(temp))
                        {
                            start = temp;
                            startSet = true;
                        }
                    }
                    if(!endSet)
                    {
                        char temp = line[last - i];
                        if(Char.IsNumber(temp))
                        {
                            end = temp;
                            endSet = true;
                        }
                    }
                    if(startSet && endSet)
                    {
                        break;
                    }
                }

                if(start == '?' || end == '?')
                {
                    throw new Exception("Number was not found on line:\n" + line);
                }

                result += int.Parse(new string([start, end]));
            }

            Console.WriteLine(result);
        }
    }
}

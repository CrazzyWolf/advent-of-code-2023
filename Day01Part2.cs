﻿using System.Collections;

namespace Advent_of_Code_2023
{
    internal class Day01Part2 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day01/input.txt");
            int result = 0;

            foreach (string line in input)
            {
                char start = '?', end = '?';
                bool startSet = false, endSet = false;
                int length = line.Length;
                int last = length - 1;
                var endBuffer = new ArrayList(5);
                var startBuffer = new ArrayList(5);

                for(int i = 0; i < length; i++)
                {
                    int j = last - i;
                    bool bufferFull = i > 3;

                    if(!startSet)
                    {
                        char c = line[i];
                        startBuffer.Add(c);

                        if(Char.IsNumber(c))
                        {
                            start = c;
                            startSet = true;
                        }
                        else
                        {
                            char r = GetDigit(startBuffer, true);
                            if(r != '?')
                            {
                                start = r;
                                startSet = true;
                            }
                            else if(bufferFull)
                            {
                                startBuffer.RemoveAt(0);
                            }
                        }
                    }
                    if(!endSet)
                    {
                        char c = line[j];
                        endBuffer.Insert(0, c);
;
                        if(Char.IsNumber(c))
                        {
                            end = c;
                            endSet = true;
                        }
                        else
                        {
                            char r = GetDigit(endBuffer, false);
                            if(r != '?')
                            {
                                end = r;
                                endSet = true;
                            }
                            else if(bufferFull)
                            {
                                endBuffer.RemoveAt(4);
                            }
                        }
                    }
                    if(startSet && endSet)
                    {
                        break;
                    }
                }

                if(start == '?' || end == '?')
                {
                    throw new Exception("Number was not found on line:\n" + line);
                }

                result += int.Parse(new string([start, end]));
            }

            Console.WriteLine(result);
        }

        private static char GetDigit(ArrayList buffer, bool endsWith)
        {
            var s = string.Join("", buffer.ToArray());
            var numbers = new Dictionary<string, char> 
            {
                { "one", '1' },
                { "two", '2' },
                { "three", '3' },
                { "four", '4' },
                { "five", '5' },
                { "six", '6' },
                { "seven", '7' },
                { "eight", '8' },
                { "nine", '9' },
            };

            if(endsWith)
            {
                foreach(var entry in numbers)
                {
                    if(s.EndsWith(entry.Key))
                    {
                        return entry.Value;
                    }
                }
            }
            else
            {
                foreach(var entry in numbers)
                {
                    if(s.StartsWith(entry.Key))
                    {
                        return entry.Value;
                    }
                }
            }

            return '?';
        }
    }
}
﻿namespace Advent_of_Code_2023
{
    internal class Day08Part1 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day08/input.txt");
            Dictionary<string, (string, string)> nodes = [];
            char[] sequence = input[0].ToCharArray();

            for(int i = 2; i < input.Length; i++)
            {
                string line = input[i];
                nodes.Add(line[..3], (line.Substring(7, 3), line.Substring(12, 3)));
            }

            int result = 0;
            string key = "AAA";
            (string l, string r) = nodes[key];
            for(int i = 0; key != "ZZZ"; i++)
            {
                key = sequence[i % sequence.Length] == 'L' ? l : r;
                (l, r) = nodes[key];
                result++;
            }

            Console.WriteLine(result);
        }
    }
}

﻿using System.Diagnostics;

namespace Advent_of_Code_2023
{
    internal class Day11 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day11/input.tar"); //example.txt

            int yLength = input.Length;
            int xLength = input[0].Length;

            int[] yMap = new int[yLength];
            int[] xMap = new int[xLength];

            int offsetY = 0;
            for(int y = 0; y < yLength; y++)
            {
                if(!input[y].Contains('#'))
                {
                    offsetY += 999999;
                }

                yMap[y] = y + offsetY;
            }

            int offsetX = 0;
            for(int x = 0; x < xLength; x++)
            {
                bool isEmpty = true;
                for(int y = 0; y < yLength; y++)
                {
                    if(input[y][x] == '#')
                    {
                        isEmpty = false;
                        break;
                    }
                }

                if(isEmpty)
                {
                    offsetX += 999999;
                }

                xMap[x] = x + offsetX;
            }

            List<(int y, int x)> galaxies = [];

            for(int y = 0; y < yLength; y++)
            {
                for(int x = 0; x < xLength; x++)
                {
                    if(input[y][x] == '#')
                    {
                        galaxies.Add((yMap[y], xMap[x]));
                    }
                }
            }

            int count = galaxies.Count;
            long result = 0;

            for(int i = 0; i < count; i++)
            {
                var (y1, x1) = galaxies[i];
                for(int j = i + 1; j < count; j++)
                {
                    var (y2, x2) = galaxies[j];
                    result += Math.Abs(y1 - y2) + Math.Abs(x1 - x2);
                }
            }

            Console.WriteLine(result);
        }
    }
}

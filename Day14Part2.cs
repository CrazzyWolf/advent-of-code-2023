﻿namespace Advent_of_Code_2023
{
    internal class Day14Part2 : IDayPart
    {
        public static void Start()
        {
            char[][] grid = File.ReadAllLines("../../../Day14/input.txt").Select(a => a.ToCharArray()).ToArray();

            Dictionary<string, int> visited = [];
            int cycle = 0;
            string key;

            while(!visited.ContainsKey(key = string.Join("", grid.Select(a => new string(a)).ToArray())))
            {
                visited[key] = cycle++;
                grid = Cycle(grid);
            }

            int start = visited[key];
            int index = start + (1000000000 - start) % (cycle - start);
            key = visited.First(a => a.Value == index).Key;

            int result = 0;
            int length = grid.Length;
            int length2 = grid[0].Length;
            for(int i = 0; i < length; i++)
            {
                int offset = i * length2;
                for(int j = 0; j < length2; j++)
                {
                    if(key[offset + j] == 'O')
                    {
                        result += length - i;
                    }
                }
            }

            Console.WriteLine(result);
        }

        private static char[][] Cycle(char[][] grid)
        {
            int length = grid.Length;
            int length2 = grid[0].Length;
            int iMax = length - 1;
            int jMax = length2 - 1;

            for(int i = 0; i < length; i++)
                for(int j = 0; j < length2; j++)
                    if(grid[i][j] == 'O')
                    {
                        int k = i;
                        while(k > 0 && grid[k - 1][j] == '.')
                        {
                            k--;
                        }
                        grid[i][j] = '.';
                        grid[k][j] = 'O';
                    }
            for(int j = 0; j < length2; j++)
                for(int i = 0; i < length; i++)
                    if(grid[i][j] == 'O')
                    {
                        int k = j;
                        while(k > 0 && grid[i][k - 1] == '.')
                        {
                            k--;
                        }
                        grid[i][j] = '.';
                        grid[i][k] = 'O';
                    }
            for(int i = iMax; i >= 0; i--)
                for(int j = 0; j < length2; j++)
                    if(grid[i][j] == 'O')
                    {
                        int k = i;
                        while(k < iMax && grid[k + 1][j] == '.')
                        {
                            k++;
                        }
                        grid[i][j] = '.';
                        grid[k][j] = 'O';
                    }
            for(int j = jMax; j >= 0; j--)
                for(int i = 0; i < length; i++)
                    if(grid[i][j] == 'O')
                    {
                        int k = j;
                        while(k < jMax && grid[i][k + 1] == '.')
                        {
                            k++;
                        }
                        grid[i][j] = '.';
                        grid[i][k] = 'O';
                    }

            return grid;
        }
    }
}

﻿using System.Text.RegularExpressions;

namespace Advent_of_Code_2023
{
    internal partial class Day06Part2 : IDayPart
    {
        [GeneratedRegex(@" +")]
        private static partial Regex MyRegex();

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day06/input.tar"); //example.txt

            long time = Parse(input[0]);
            long distance = Parse(input[1]);

            long start = 0, end = 0;
            for(long chargeTime = 1; chargeTime < time; chargeTime++)
            {
                if((time - chargeTime) * chargeTime < distance)
                {
                    start = chargeTime;
                }
                else
                {
                    break;
                }
            }

            for(long chargeTime = time - 1; chargeTime > 0; chargeTime--)
            {
                if((time - chargeTime) * chargeTime < distance)
                {
                    end = chargeTime;
                }
                else
                {
                    break;
                }
            }

            Console.WriteLine(end - start - 1);
        }

        private static long Parse(string input)
        {
            return long.Parse(string.Join("", MyRegex().Split(input).Skip(1)));
        }
    }
}
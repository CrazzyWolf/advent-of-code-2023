﻿namespace Advent_of_Code_2023
{
    internal class Day07Part2 : IDayPart
    {
        public enum HandType
        {
            HighCard,
            OnePair,
            TwoPair,
            ThreeOfAKind,
            FullHouse,
            FourOfAKind,
            FiveOfAKind,
        }

        class HandComparer : IComparer<(string hand, HandType type)>
        {
            static readonly Dictionary<char, int> cardTypes = new()
            {
                { 'J', 0 },
                { '2', 1 },
                { '3', 2 },
                { '4', 3 },
                { '5', 4 },
                { '6', 5 },
                { '7', 6 },
                { '8', 7 },
                { '9', 8 },
                { 'T', 9 },
                { 'Q', 10 },
                { 'K', 11 },
                { 'A', 12 },
            };

            public int Compare((string hand, HandType type) a, (string hand, HandType type) b)
            {
                int c1 = a.type.CompareTo(b.type);
                if(c1 != 0)
                {
                    return c1;
                }

                string aHand = a.hand, bHand = b.hand;
                for(int i = 0; i < aHand.Length; i++)
                {
                    int c2 = cardTypes[aHand[i]] - cardTypes[bHand[i]];
                    if(c2 != 0)
                    {
                        return c2;
                    }
                }

                return 0;
            }
        }

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day07/input.tar"); //example.txt

            List<(string hand, int bid, HandType type)> hands = [];

            foreach(string line in input)
            {
                string[] split = line.Split(" ");
                string hand = split[0];
                hands.Add((hand, int.Parse(split[1]), GetHandType(hand)));
            }

            int[] orderedBids = hands.OrderBy(x => (x.hand, x.type), new HandComparer()).Select(x => x.bid).ToArray();

            int result = 0;
            for(int rank = 1; rank <= orderedBids.Length; rank++)
            {
                result += rank * orderedBids[rank - 1];
            }

            Console.WriteLine(result);
        }

        private static readonly Dictionary<int, HandType> numToType = new()
        {
            { 1, HandType.HighCard },
            { 2, HandType.OnePair },
            { 3, HandType.ThreeOfAKind },
            { 4, HandType.FourOfAKind },
            { 5, HandType.FiveOfAKind },
        };

        private static HandType GetHandType(string hand)
        {
            Dictionary<char, int> dict = [];
            foreach(char card in hand)
            {
                dict.TryGetValue(card, out int value);
                dict[card] = value + 1;
            }

            dict.TryGetValue('J', out int jokers);
            var ordered = dict.OrderBy(x => x.Value).Reverse().ToArray();
            int length = ordered.Length;
            int temp = jokers + ordered[0].Value;

            if(length == 3)
            {
                if(jokers == 0 && ordered[0].Value == 2)
                {
                    return HandType.TwoPair;
                }

                if(jokers == 3)
                {
                    return HandType.FourOfAKind;
                }
            }

            if(temp == 3 && (length == 2 || jokers == 1 && length == 3))
            {
                return HandType.FullHouse;
            }

            if(jokers == 2 && length == 4)
            {
                return HandType.ThreeOfAKind;
            }

            return numToType[Math.Min(5, temp)];
        }
    }
}
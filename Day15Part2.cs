﻿namespace Advent_of_Code_2023
{
    internal class Day15Part2 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day15/input.txt")[0].Split(",");
            int size = 256;
            var boxes = new Dictionary<string, int>[size];
            var order = new List<string>[size];

            for(int i = 0; i < size; i++)
            {
                boxes[i] = [];
                order[i] = [];
            }

            foreach(string line in input)
            {
                bool remove = line.Last() == '-';
                string[] split = line.Split(remove ? '-' : '=');
                string key2 = split[0];
                int key = Hash(key2);

                if(remove)
                {
                    boxes[key].Remove(key2);
                    order[key].Remove(key2);
                }
                else
                {
                    if(!boxes[key].ContainsKey(key2))
                    {
                        order[key].Add(key2);
                    }
                    boxes[key][key2] = int.Parse(split[1]);
                }
            }

            int result = 0;
            for(int i = 0; i < size; i++)
            {
                var o = order[i];
                for(int j = 1; j <= o.Count; j++)
                {
                    string key = o[j - 1];
                    result += (Hash(key) + 1) * j * boxes[i][key];
                }
            }
            Console.WriteLine(result);
        }

        private static int Hash(string s)
        {
            return s.Aggregate(0, (a, b) => 17 * (a + b) % 256);
        }
    }
}

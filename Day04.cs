﻿using System.Collections;

namespace Advent_of_Code_2023
{
    internal class Day04 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day04/input.tar");

            int result = 0;
            int[] wins = new int[input.Length];
            int[] cards = new int[input.Length];
            int[] points = new int[input.Length];

            for(int i = 0; i < input.Length; i++)
            {
                string[] split = input[i].Split(": ")[1].Split(" | ");
                int[] numbers = Parse(split[1]).ToArray();
                Dictionary<int, int> winningNumbers = Parse(split[0]).ToDictionary(x => x, x => 0);

                foreach(int number in numbers)
                {
                    if(winningNumbers.TryGetValue(number, out int value))
                    {
                        winningNumbers[number] = ++value;
                    }
                }

                int win = wins[i] = winningNumbers.Sum(x => x.Value);
                if(win > 0)
                {
                    points[i] = (int)Math.Pow(2, win - 1);
                }
            }

            Console.WriteLine(points.Sum(x => x));

            for(int i = 0; i < cards.Length; i++)
            {
                int instance = cards[i] + 1;
                result += instance;

                int offset = i + 1;
                int max = Math.Min(wins[i] + offset, cards.Length);
                for(int j = offset; j < max; j++)
                {
                    cards[j] += instance;
                }
            }

            Console.WriteLine(result);
        }

        private static IEnumerable<int> Parse(string input)
        {
            return input.Split(" ").Where(v => v.Length > 0).Select(int.Parse);
        }
    }
}

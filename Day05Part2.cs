﻿using System.Diagnostics;

namespace Advent_of_Code_2023
{
    internal class Day05Part2 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day05/input.tar");

            long[] seedRanges = input[0].Remove(0, 7).Split(" ").Select(long.Parse).ToArray();
            List<(long, long)> seeds = [];

            for(int i = 0; i < seedRanges.Length; i += 2)
            {
                long start = seedRanges[i];
                long end = start + seedRanges[i + 1] - 1;
                seeds.Add((start, end));
            }

            long result = long.MaxValue;
            List<List<(long, long, long)>> maps = [];
            List<(long, long, long)> tempMap = [];

            for(int i = 3; i <= input.Length; i++)
            {
                if(i == input.Length || input[i].Length == 0)
                {
                    maps.Add(tempMap);
                    tempMap = [];
                    i++;
                }
                else
                {
                    long[] split = input[i].Split(" ").Select(long.Parse).ToArray();
                    tempMap.Add((split[0], split[1], split[2]));
                }
            }

            foreach(var seed in seeds)
            {
                Stack<(long, long)> temp1 = [];
                temp1.Push(seed);
                foreach(var map in maps)
                {
                    Stack<(long, long)> temp2 = [];
                    while(temp1.Count > 0)
                    {
                        (long start, long end) = temp1.Pop();
                        bool change = false;

                        foreach((long dStart, long sStart, long range) in map)
                        {
                            long sEnd = sStart + range;
                            long offset = dStart - sStart;
                            bool tempChange = true;

                            if(start >= sStart && end <= sEnd) //je v rozsahu celej
                            {
                                temp2.Push((start + offset, end + offset));
                            }
                            else if(start < sStart && end >= sStart && end < sEnd) //je castecne v rozsahu zleva
                            {
                                temp1.Push((start, sStart - 1));
                                temp2.Push((sStart + offset, end + offset));
                            }
                            else if(start > sStart && start <= sEnd && end > sEnd) //je castecne v rozsahu zprava
                            {
                                temp2.Push((start + offset, sEnd + offset));
                                temp1.Push((sEnd + 1, end));
                            }
                            else if(start < sStart && end > sEnd) //je pres rozsah z obou stran
                            {
                                temp1.Push((start, sStart - 1));
                                temp2.Push((sStart + offset, sEnd + offset));
                                temp1.Push((sEnd + 1, end));
                            }
                            else //není v rozsahu
                            {
                                tempChange = false;
                            }

                            if(tempChange)
                            {
                                change = true;
                            }
                        }

                        if(!change) //není v žádném rozsahu
                        {
                            temp2.Push((start, end));
                        }
                    }

                    temp1 = temp2;
                }

                result = Math.Min(result, temp1.Min(x => x.Item1));
            }

            Console.WriteLine(result);
        }
    }
}
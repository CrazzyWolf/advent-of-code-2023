﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advent_of_Code_2023
{
    internal class Day02Part1OneLine : IDayPart
    {
        public static void Start()
        {
            Console.WriteLine(File.ReadAllLines("../../../Day02/input.txt").Select(value => (int.Parse(value.Split(": ")[0].Remove(0, 5)), value.Split(": ")[1].Split("; ").Where(set => set.Split(", ").Where(color => int.Parse(color.Split(" ")[0]) <= new Dictionary<string, int>() {{ "red", 12 }, { "green", 13 }, { "blue", 14 }}[color.Split(" ")[1]]).ToArray().Length == set.Split(", ").Length).ToArray().Length == value.Split(": ")[1].Split("; ").Length)).Sum(game => game.Item2 ? game.Item1 : 0));
        }
    }
}

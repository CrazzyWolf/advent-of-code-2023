﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advent_of_Code_2023
{
    internal class Day02Part1 : IDayPart
    {
        static readonly Dictionary<string, int> maxColors = new()
        {
            { "red", 12 },
            { "green", 13 },
            { "blue", 14 },
        };

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day02/input.txt");
            int result = 0;

            for(int gameIndex = 0; gameIndex < input.Length; gameIndex++)
            {
                string[] sets = input[gameIndex].Split(": ")[1].Split("; ");
                foreach(string set in sets)
                {
                    string[] colorAmount = set.Split(", ");
                    foreach(string color in colorAmount)
                    {
                        string[] colorSplit = color.Split(" ");
                        if(maxColors[colorSplit[1]] < int.Parse(colorSplit[0]))
                        {
                            goto endGame;
                        }
                    }
                }

                result += gameIndex + 1;
                endGame:;
            }

            Console.WriteLine(result);
        }
    }
}

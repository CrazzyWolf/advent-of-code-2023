﻿namespace Advent_of_Code_2023
{
    internal class Day09 : IDayPart
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day09/input.txt");

            long part1 = 0;
            long part2 = 0;

            foreach(string line in input)
            {
                long[] last = line.Split(' ').Select(long.Parse).ToArray();
                List<long> firstArray = [];

                while(last.Where(x => x != 0).Any())
                {
                    firstArray.Add(last[0]);
                    part1 += last.Last();

                    long[] sequence = new long[last.Length - 1];
                    for(int i = 0; i < sequence.Length; i++)
                    {
                        sequence[i] = last[i + 1] - last[i];
                    }

                    last = sequence;
                }

                long temp = 0;
                for(int i = firstArray.Count - 1; i >= 0; i--)
                {
                    temp = firstArray[i] - temp;
                }
                part2 += temp;
            }

            Console.WriteLine(part1);
            Console.WriteLine(part2);
        }
    }
}

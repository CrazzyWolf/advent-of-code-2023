﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Advent_of_Code_2023
{
    internal class Day03Part2 : IDayPart
    {
        private static readonly string[] input = File.ReadAllLines("../../../Day03/input.txt");
        private static readonly List<(int x, int y, int result)> gears = [];

        public static void Start()
        {

            for(int y = 0; y < input.Length; y++)
            {
                int tempX = 0;
                bool isNumber = false;
                char[] line = input[y].ToCharArray();
                for(int x = 0; x < line.Length + 1; x++)
                {
                    if(x != line.Length && char.IsDigit(line[x]))
                    {
                        if(!isNumber)
                        {
                            isNumber = true;
                            tempX = x;
                        }
                    }
                    else if(isNumber)
                    {
                        AddNumber(tempX, x - 1, y);
                        isNumber = false;
                    }
                }
            }

            Dictionary<string, (int result, int adjacentNumbers)> resultDict = [];
            foreach(var (x, y, result) in gears)
            {
                string key = $"{y}-{x}";
                if(resultDict.TryGetValue(key, out (int result, int adjacentNumbers) gear))
                {
                    gear.result *= result;
                    gear.adjacentNumbers++;
                    resultDict[key] = gear;
                }
                else
                {
                    resultDict.Add(key, (result, 1));
                }
            }

            Console.WriteLine(resultDict.Sum(v => v.Value.adjacentNumbers == 2 ? v.Value.result : 0));
        }

        private static void AddNumber(int x1, int x2, int y)
        {
            int i1 = y - 1;
            int i2 = y + 2;
            int j1 = x1 - 1;
            int j2 = x2 + 2;

            for(int i = i1; i < i2; i++)
            {
                if(i > 0 && i < input.Length)
                {
                    string line = input[i];
                    for(int j = j1; j < j2; j++)
                    {
                        if(j > 0 && j < line.Length && line[j] == '*')
                        {
                            gears.Add((i, j, int.Parse(input[y].Substring(x1, x2 - x1 + 1))));
                            return;
                        }
                    }
                }
            }
        }
    }
}
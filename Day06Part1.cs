﻿using System.Text.RegularExpressions;

namespace Advent_of_Code_2023
{
    internal partial class Day06Part1 : IDayPart
    {
        [GeneratedRegex(@" +")]
        private static partial Regex MyRegex();

        public static void Start()
        {
            string[] input = File.ReadAllLines("../../../Day06/input.tar"); //example.txt

            string[] timeSplit = MyRegex().Split(input[0]);
            string[] distanceSplit = MyRegex().Split(input[1]);

            List<(int, int)> races = [];

            for(int i = 1; i < timeSplit.Length; i++)
            {
                races.Add((int.Parse(timeSplit[i]), int.Parse(distanceSplit[i])));
            }

            int[] results = new int[races.Count];

            for(int i = 0; i < races.Count; i++)
            {
                (int time, int distance) = races[i];
                for(int chargeTime = 1; chargeTime < time; chargeTime++)
                {
                    if((time - chargeTime) * chargeTime > distance)
                    {
                        results[i]++;
                    }
                }
            }

            Console.WriteLine(results.Aggregate(1, (a, b) => a * b));
        }
    }
}